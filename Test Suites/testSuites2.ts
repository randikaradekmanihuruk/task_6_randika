<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>testSuites2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9469f2d1-c233-4565-8b96-fb2d3757e7d0</testSuiteGuid>
   <testCaseLink>
      <guid>defbc3cb-99bf-49df-9cfd-4a5cf59e9c6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CheckoutDataDB</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>cf3c8fee-d887-4533-814e-3b314d7a74ce</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataCheckout</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>cf3c8fee-d887-4533-814e-3b314d7a74ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>FirstName</value>
         <variableId>83fd9480-7b3c-49aa-ae6b-cd98d375db85</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>cf3c8fee-d887-4533-814e-3b314d7a74ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Last Name</value>
         <variableId>2fc50e69-1f97-4be0-941e-938f5222909f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>cf3c8fee-d887-4533-814e-3b314d7a74ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ZipCode</value>
         <variableId>bb9b51e9-3fbf-41e6-9f81-51ed044f8c01</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
